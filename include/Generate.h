#include <utility>

/*
* @Author: Lin Sinan
* @Github: https://github.com/linsinan1995
* @Email: mynameisxiaou@gmail.com
 * @LastEditors: Lin Sinan
* @Description: 
*               
* generate a maze with following feature:
* 1. a consequential obstacles that is slightly small the maximun range of maze;            
* 2. some random obstacles   
* 3. 1 -> obstacle; 0 -> accessible path
*/


#ifndef ASTAR_GENERATOR_H
#define ASTAR_GENERATOR_H

#include <fstream>
#include <iostream>
#include <random>
// #include <utility>
#include <vector>
#include <algorithm>

typedef std::mt19937 Engine;

class MyRng
{
    Engine engine;
    std::uniform_int_distribution<int> gWidth;
    std::uniform_int_distribution<int> gHeight;
public:
    MyRng() = default;

    MyRng(int width, int height, int seed) : gWidth(0, width-1), gHeight(0, height-1)
    {
        engine.seed(seed);
    }

    MyRng(int widthMin, int widthMax, int heightMin, int heightMax, int seed) : 
        gWidth(widthMin, widthMax), gHeight(heightMin, heightMax)
    {
        engine.seed(seed);
    }
    
    std::pair<int, int> get()
    {
        return { gWidth(engine), gHeight(engine)};
    }
    
};

class Generator
{
    MyRng rng;
    int width, height;
    std::string pathToData;
    std::vector<std::pair<int, int>> obstacleLocationVector {};
public:
    explicit Generator(int width = 10, int height = 10, std::string  pathToData = "../data/maze", int seed = 0);
    void generate(int obstacleNumber = 10);
private:
    void generateObstacles(int obstacleNumber);
    void duplicatesProcess(int obstacleNumber);
};

inline
Generator::Generator(int width, int height, std::string  pathToData, int seed) :
        width(width), height(height), rng(width, height, seed), pathToData(std::move(pathToData))
{
}

inline
void Generator::generateObstacles(int obstacleNumber)
{
    for (int i = 0; i < obstacleNumber; i++)
    {
        obstacleLocationVector.push_back(rng.get());
    }

    auto predicate = [](const std::pair<int, int>& p1, const std::pair<int, int>& p2){
        return p1 > p2;
    };

    std::sort(obstacleLocationVector.begin(), obstacleLocationVector.end(), predicate);
    this->duplicatesProcess(obstacleNumber);

#ifdef G_TEST
    for_each(obstacleLocationVector.begin(), obstacleLocationVector.end(), [](const std::pair<int, int>& p){
        std::cout << p.first << "," << p.second << "\n";
    });
    std::cout << "----------------------------" << std::endl;
#endif
}

inline
void Generator::generate(int obstacleNumber)
{
    this->generateObstacles(obstacleNumber);

    std::ofstream out(pathToData);
    if (!out.is_open()) std::cout << "an error occurs when writing a file!\n"; 
    
    int cnt = 0;
    auto [x, y] = obstacleLocationVector.back(); obstacleLocationVector.pop_back();

    for (int i = 0; i < height; i++) 
    {
        for (int j = 0; j < width; j++) 
        {
            std::cout << i << " " << j << std::endl;
            if (i == x && j == y) 
            {
                cnt ++;
                out << "1 ";
#ifdef G_TEST
                std::cout << "----------------------------" << std::endl;
                std::cout << "\t\tCATCHA" << std::endl;
                std::cout << "----------------------------" << std::endl;
#endif
                if (!obstacleLocationVector.empty())
                    std::tie(x, y) = obstacleLocationVector.back(); obstacleLocationVector.pop_back();
            }
            else out << "0 ";
        }
        out << "\n";
    }
    /*
        out.close(); 
        fstream is a proper RAII object, it does close automatically at the end of the scope, 
        and there is absolutely no need whatsoever to call close manually when closing at the 
        end of the scope is sufficient.
    */
#ifdef G_TEST
    std::cout << "Obstacles number is " << cnt << std::endl;
    std::cout << "It supposed to be " << obstacleNumber << std::endl;
#endif
}

inline
void Generator::duplicatesProcess(int obstacleNumber) 
{
    int i = 1;
    for (int j = 1; j < obstacleLocationVector.size(); ++j) 
    {
        if (obstacleLocationVector[i - 1] != obstacleLocationVector[j]) 
        {
            obstacleLocationVector[i++] = obstacleLocationVector[j];
        } 
    }
    for (auto i : obstacleLocationVector) std::cout << i.first << ", " << i.second << std::endl;

    auto endIter = obstacleLocationVector.begin()+i;
    obstacleLocationVector.erase(endIter, obstacleLocationVector.end());
    for (auto i : obstacleLocationVector) std::cout << i.first << ", " << i.second << std::endl;

}

#endif


// todo 1. Generate different number; 2. change pair to point class;
